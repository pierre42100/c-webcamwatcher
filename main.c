#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <limits.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <string.h>
#include <time.h>

#define WEBCAM_PATH "/dev/video0"

static void showAlert() {
    
    Display *d = NULL;
    int s;
    Window w;
    
    XEvent e;
    const char *msg = "Webcam used!";
    char msg2[80];
    const char *msg3 = "Press a key to close this window";
    
    // Get current date
    for(int i = 60; i > 0; i--) msg2[i] = '\0';
    time_t t = time(NULL);
    ctime_r(&t, msg2);


    // Initialize XDisplay
    d = XOpenDisplay(NULL);
    if (d == NULL) {
        fprintf(stderr, "Cannot open display\n");
        exit(1);
    }

    s = DefaultScreen(d);

    w = XCreateSimpleWindow(d, RootWindow(d, s), 10, 10, 220, 100, 1,
                        BlackPixel(d, s), WhitePixel(d, s));
    XSelectInput(d, w, ExposureMask | KeyPressMask);

    
    
    XMapWindow(d, w);

    while (1) {
        XNextEvent(d, &e);
        if (e.type == Expose) {
            XFillRectangle(d, w, DefaultGC(d, s), 20, 20, 10, 10);
            XDrawString(d, w, DefaultGC(d, s), 10, 50, msg, strlen(msg));
            XDrawString(d, w, DefaultGC(d, s), 10, 70, msg2, strlen(msg2));
            XDrawString(d, w, DefaultGC(d, s), 10, 90, msg3, strlen(msg3));
        }
        if (e.type == KeyPress)
            break;
    }

    XDestroyWindow(d, w);
    XCloseDisplay(d);
}

int main(int argc, char**argv) {
    
    char *path = WEBCAM_PATH;

    printf("Watch %s...\n", path);

    int inotifyFd = inotify_init();
    if(inotifyFd == -1) {
        perror("inotify_init");
        exit(-1);
    }

    if(inotify_add_watch(inotifyFd, path, IN_OPEN) == -1) {
        perror("inotify_add_watch");
        exit(-2);
    }

    struct inotify_event event;
    for(;;) {
        int numread = read(inotifyFd, &event, sizeof(struct inotify_event));

        if(numread == 0)
            continue;
        
        if(numread == -1) {
            perror("read");
            exit(-2);
        }

        showAlert();
    }

    return 0;
}